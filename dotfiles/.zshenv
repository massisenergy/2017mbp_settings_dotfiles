#!/usr/bin/env zsh

# Setting PATH for Python 3.6
# The original version is saved in .bash_profile.pysave
PATH="/Library/Frameworks/Python.framework/Versions/3.6/bin:${PATH}"
export PATH

# added by Anaconda3 5.2.0 installer
# export PATH="$HOME/anaconda3/bin:$PATH"

##
# Your previous $HOME/.bash_profile file was backed up as $HOME/.bash_profile.macports-saved_2019-05-19_at_17:46:36
##

# MacPorts Installer addition on 2019-05-19_at_17:46:36: adding an appropriate PATH variable for use with MacPorts.
#export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
export PATH="/bin:/sbin:$PATH" #M1PRO
# Finished adapting your PATH environment variable for use with MacPorts.

# Pop this into your ~/.bashrc (Linux) or ~/.bash_profile (Mac)
dock_ip(){
	docker inspect --format '{{ .NetworkSettings.IPAddress }}' "$@"
}
# And complie by invoking 'source ~/.bash_profile' in terminal
#export PATH="/usr/local/sbin:$PATH"
export PATH="/usr/sbin:$PATH" #M1PRO

#PATH="/usr/local/opt/grep/libexec/gnubin:$PATH" #enable gnu grep Homebrew
PATH="/opt/homebrew/Cellar/grep/3.7/libexec/gnubin:$PATH" #M1PRO
PATH="/opt/homebrew/opt/make/libexec/gnubin:$PATH" #M1PRO
export PATH="/opt/homebrew/opt/flex/bin:$PATH" #M1PRO

alias showFiles='defaults write com.apple.finder AppleShowAllFiles YES; killall Finder /System/Library/CoreServices/Finder.app'
alias hideFiles='defaults write com.apple.finder AppleShowAllFiles NO; killall Finder /System/Library/CoreServices/Finder.app'
alias L='lsd -lrh'; alias LA='L -a'; alias LR='L -R'; alias LAR='L -aR';
alias LS='ls -Gplrsh'; alias gits='git status'
alias oA='open -a ';
alias iplocal="echo $(ifconfig -a | grep -E 'UP|inet[^6]' | grep -A1 UP | grep inet | grep -v 127 | tail -1 | awk '{print $2}')"
alias Gmsh='open -a $HOME/Applications/Gmsh/Gmsh.app/'
alias Atom='open -a $HOME/Applications/Atom.app'
alias airport='/System/Library/PrivateFrameworks/Apple80211.framework/Versions/A/Resources/airport scan'
alias FreeCAD='$HOME/Applications/FreeCAD.app/Contents/Resources/bin/FreeCAD'
alias FC='$HOME/Applications/FreeCAD.app/Contents/Resources/bin/FreeCAD'
alias FreeCADCmd='$HOME/Applications/FreeCAD.app/Contents/Resources/bin/FreeCADCmd'
alias FCC='$HOME/Applications/FreeCAD.app/Contents/Resources/bin/FreeCADCmd'
alias paraView='open -a $HOME/Applications/ParaView-5.8.0.app'
alias hl=highlight; alias hlC++='highlight --syntax=C++ -i ';
alias hlZellner='hl --syntax=C++ --style="/usr/local/Cellar/highlight/4.1/share
/highlight/themes/zellner.theme" -i';

CDo=$HOME/ownCloud
CDG=$HOME/ownCloud/Gmsh
CDF=$HOME/ownCloud/FreeCAD
CDE=$HOME/ownCloud/openFoamDataSR/E3DBP_Nozzle
CDD=$HOME/Downloads
CDP=$HOME/Sync/Coding
CDR=$HOME/Sync/Coding/R
CDC=$HOME/Sync/Coding/C++
CDS=$HOME/Sync
CDI=$HOME/Library/Mobile\ Documents/com~apple~CloudDocs
ICLOUD=$HOME/Library/Mobile\ Documents/com~apple~CloudDocs
CDDOE=$HOME/Sync/2019PD/2019PD_Project_Sourav/E3DPB/DOE_ERR_SM_LG_paper
CDPAA=$HOME/Sync/2019PD/2019PD_Project_Sourav/E3DPB/CFD_E3DBP_PauloA_SM_MA_LG_VB

#openFOAM_v1912 specific
alias of_v1912='source $HOME/ownCloud/of_v1912_openFOAM_com/startMacOpenFOAM.dms';
alias procFoaming='source $HOME/ownCloud/openFoamDataSR/Scripts_back-up/processor_foaming_for_paraView.sh'

alias vncviewer='/Applications/RealVNC/VNC\ Viewer.app/Contents/MacOS/vncviewer ';
export RSTUDIO_PANDOC=$HOME/Applications/RStudio.app/Contents/MacOS/pandoc
export PATH="/usr/local/opt/libarchive/bin:$PATH"
export PATH="/usr/local/opt/libarchive/bin:$PATH"
# alias GC=$HOME/Applications/GoldenCheetah.app/Contents/MacOS/GoldenCheetah --no-r &
source ~/.iterm2_shell_integration.zsh
source ~/.iterm2_shell_integration.zsh
alias LT='L --tree'
alias paraview='/Applications/ParaView-5.9.1.app/Contents/MacOS/paraview'

export NVM_DIR="$HOME/.nvm"
  [ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && \. "/opt/homebrew/opt/nvm/nvm.sh"  # This loads nvm
  [ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && 
      \. "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion

# Setting for the new UTF-8 terminal support in Lion
LC_CTYPE=en_US.UTF-8
LC_ALL=en_US.UTF-8
export MONO_GAC_PREFIX='/usr/local'
export PATH="/usr/local/opt/openjdk/bin:$PATH"
export CPPFLAGS="-I/usr/local/opt/openjdk/include"
paraview=/Applications/ParaView-5.9.1.app/Contents/MacOS/paraview
PATH="/usr/local/opt/findutils/libexec/gnubin:$PATH"
PATH="/usr/local/opt/gnu-tar/libexec/gnubin:$PATH"
PATH="/usr/local/opt/gnu-sed/libexec/gnubin:$PATH"
PATH="/usr/local/opt/gnu-indent/libexec/gnubin:$PATH"
PATH="/usr/local/opt/findutils/libexec/gnubin:$PATH"
PATH="/usr/local/opt/gnu-tar/libexec/gnubin:$PATH"
PATH="/usr/local/opt/gnu-sed/libexec/gnubin:$PATH"
PATH="/usr/local/opt/gnu-indent/libexec/gnubin:$PATH"
alias pVClient_Server_localhost3333='paraview -url=cs://localhost:3333 &'
alias pvpython='/Applications/ParaView-5.9.1.app/Contents/bin/pvpython'
alias pVClient_Server_localhost2222='paraview -url=cs://localhost:2222 &'

#export FOAM_INST_DIR=/Volumes/OpenFOAM
source /Volumes/OpenFOAM/OpenFOAM-v2106/etc/bashrc
export PATH="/opt/homebrew/opt/flex/bin:$PATH"
export PATH="/opt/homebrew/opt/m4/bin:$PATH"
